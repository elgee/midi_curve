Simple standalone JACK aplication to transform the velocities of midi notes in real time according to a user adjustable curve.

Requires a running JACK session and a JACK patchbay application (e.g. Catia) to connect the midi ports.
