BIN = midi_curve
SRC_DIRS = ./src
BUILD_DIR = ./build

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS = $(INC_FLAGS) -MMD -MP -g -std=c++17
LDLIBS = -lglfw -lGL -lm -lGLU -lGLEW -ljack

NK_DEFS = NK_INCLUDE_FIXED_TYPES\
		  NK_INCLUDE_STANDARD_IO\
		  NK_INCLUDE_STANDARD_VARARGS\
		  NK_INCLUDE_DEFAULT_ALLOCATOR\
		  NK_INCLUDE_VERTEX_BUFFER_OUTPUT\
		  NK_INCLUDE_FONT_BAKING\
		  NK_INCLUDE_DEFAULT_FONT\
		  NK_KEYSTATE_BASED_INPUT

$(BIN): $(OBJS)
	g++ $(LDFLAGS) $(OBJS) -o $@ $(LDLIBS)

$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	g++ $(CPPFLAGS) $(CFLAGS) $(addprefix -D,$(NK_DEFS)) -c $< -o $@

$(BUILD_DIR)/%.cpp.o: %.cpp
	mkdir -p $(dir $@)
	g++ $(CPPFLAGS) $(CFLAGS) $(addprefix -D,$(NK_DEFS)) -c $< -o $@

clean:
	rm -r $(BUILD_DIR)
	
-include $(DEPS)
