#include <jack/jack.h>
#include <jack/midiport.h>
#include <cstddef>
#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "midi_curve.h"
#include "jack_client.h"

#include <array>
#include <vector>
#include <algorithm>
#include <memory>

#include "nuklear.h"
#include "nuklear_glfw_gl4.h"
#include "nk_graph.h"
#include "limited_slider.h"

#define MAX_VERTEX_BUFFER 512 * 1024
#define MAX_ELEMENT_BUFFER 128 * 1024

jack_client_t* client;
jack_port_t*   input;
jack_port_t*   output;

int main()
{
	std::shared_ptr<MidiCurve> velocity_curve{ std::make_shared<MidiCurve>() };
	JackClient client{ velocity_curve };
	client.init();
	auto log_queue{ std::make_shared<RBLogger::log_buffer_t>() };
	client.logger.set_output(log_queue);
	client.start_processing();


    static GLFWwindow *win;
    int width = 0, height = 0;
    struct nk_context *ctx;
    
    const int WINDOW_WIDTH{ 250 };
    const int WINDOW_HEIGHT{ 315 };

    // set up GLFW window
    if (!glfwInit()) {
        throw("GLFW failed to init\n");
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE,GLFW_FALSE);
    win = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Midicurve", NULL, NULL);
    glfwMakeContextCurrent(win);
    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    glewExperimental = 1;
    if (glewInit() != GLEW_OK)
        throw("Failed to setup GLEW.\n");

    ctx = nk_glfw3_init(win, NK_GLFW3_INSTALL_CALLBACKS, MAX_VERTEX_BUFFER, MAX_ELEMENT_BUFFER);
    {struct nk_font_atlas *atlas;
    nk_glfw3_font_stash_begin(&atlas);
    struct nk_font *roboto = nk_font_atlas_add_from_file(atlas, "Roboto-Regular.ttf", 18, 0);
    nk_glfw3_font_stash_end();
    nk_style_set_font(ctx, &roboto->handle);}

	// create graph from array of points 
    struct graph g { {0, 0, 0, 0}, nullptr, 0 };
	std::array<float, 128> x_points;
	std::array<float, 128> y_points;
	for (size_t i = 0; i < x_points.size(); ++i) {
		x_points[i] = static_cast<float>(i);
		y_points[i] = static_cast<float>(velocity_curve->adjust_volume(static_cast<midi_data_t>(i)));
	}
	if (make_graph(&g, x_points.data(), y_points.data(), 128, 0, 0, 128, 128))
		throw("Failed to make graph\n.");

	bool do_draw{ true }; 
	int show_input{ 0 };
	int log_history_count{ 0 };
	int log_history_max{ 5 };
	struct nk_vec2 log_history[log_history_max];
    while (!glfwWindowShouldClose(win))
    {
		show_input ? glfwPollEvents() : glfwWaitEvents();
		do_draw = true;
        nk_glfw3_new_frame();

        // gui
        if (nk_begin(ctx, "MidiCurve", nk_rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT),
            NK_WINDOW_BORDER|NK_WINDOW_MOVABLE))
        {
			// draw sliders
            static float k = 0;
			static int min_out = 0;
			static int max_out = 127;
			static int max_in = 127;
			const float label_slider_ratio[] = { 0.25, 0.75 };
			nk_layout_row(ctx, NK_DYNAMIC, 0, 2, label_slider_ratio);
			nk_label(ctx, "Curvature", NK_TEXT_RIGHT);
			if (nk_slider_float(ctx, -0.1, &k, 0.1, 0.005))
				velocity_curve->set_adjustable_parameter(k);
			// for the max/min sliders ensure that max >= min. Discard the frame which 
			// shows the slider outside of these limits.
			nk_label(ctx, "Min Out", NK_TEXT_RIGHT);
			if (nk_slider_int(ctx, 0, &min_out, 127, 1)) {
				if (min_out > max_out) {
					min_out = max_out;
					do_draw = false;
				}
				velocity_curve->set_min_out(static_cast<midi_data_t>(min_out));
			}
			nk_label(ctx, "Max Out", NK_TEXT_RIGHT);
			if (nk_slider_int(ctx, 0, &max_out, 127, 1)) {
				if (max_out <= min_out) {
					max_out = min_out;
					do_draw = false;
				}
				velocity_curve->set_max_out(static_cast<midi_data_t>(max_out));
			}
			nk_label(ctx, "Max In", NK_TEXT_RIGHT);
			if (nk_slider_int(ctx, 0, &max_in, 127, 1))
				velocity_curve->set_max_in(static_cast<midi_data_t>(max_in));

			// draw graph
            nk_layout_row_dynamic(ctx, 100, 1);
			velocity_curve->calculate_curve();
			if (velocity_curve->update_curve()) {
				for (midi_data_t i = 0; i < 128; ++i) {
					y_points[i] = static_cast<float>(velocity_curve->adjust_volume(i));
					update_y_coordinates(&g, y_points.data());
				}
			}
			if (show_input) {
				for (int i = 0; i < log_queue->count(); ++i) {
					RBLogger::midi_change_t mc = log_queue->pop();
					struct nk_vec2 point{ static_cast<float>(mc.first), static_cast<float>(mc.second) };
					log_history[log_history_count % log_history_max] = point;
					++log_history_count;
				}
			}
			nk_do_graph(ctx, &g, show_input, log_history, log_history_max, (log_history_count - 1) % log_history_max);
			// draw option checkboxes/properties
			float option_ratios[] = { 0.30, 0.5, 0.2 };
            nk_layout_row(ctx, NK_DYNAMIC, 0, 3, option_ratios);
			if (nk_checkbox_label(ctx, "Show", &show_input)) {		
				// set now but show on next update
				client.set_logging(show_input);
			}
			static int midi_channel;
			static int edit_all_channels;
			nk_property_int(ctx, "Midi Ch.", 0, &midi_channel, 15, 1, 0.1);
			nk_checkbox_label(ctx, "All", &edit_all_channels);
			if (edit_all_channels)
				client.set_midi_channel(-1);
			else
				client.set_midi_channel(midi_channel);

		}
        nk_end(ctx);

        // draw
		if (do_draw)
		{
			glfwGetWindowSize(win, &width, &height);
			glViewport(0, 0, width, height);
			glClear(GL_COLOR_BUFFER_BIT);
			nk_glfw3_render(NK_ANTI_ALIASING_ON);
			glfwSwapBuffers(win);
		}
		else
			nk_clear(ctx);
    }
	if (g.points) std::free(g.points);
    nk_glfw3_shutdown();
    glfwTerminate();
    return 0;
}
