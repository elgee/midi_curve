#include "jack_client.h"
#include "midi_curve.h"
#include <memory>
#include <functional>

JackClient::JackClient(std::shared_ptr<MidiCurve> &velocity_curve) : 
						   m_velocity_curve{ velocity_curve }, 
						   m_client{ NULL },
						   m_input{ NULL },
						   m_output{ NULL },
						   m_status{},
						   m_logging{ false },
						   m_midi_channel{ -1 }
{
}

JackClient::~JackClient()
{
	if (m_client != NULL)
		jack_client_close(m_client);
}

// Open the jack client and register the input and ouput ports
void JackClient::init() 
{
	m_client = jack_client_open("midi_curve", JackNullOption, &m_status, NULL);
	// TODO look into how status can be used to make this more helpful
	if (m_client == NULL)
		throw("Can't create jack client.\n");
	m_input = jack_port_register(m_client, "midi in", JACK_DEFAULT_MIDI_TYPE, 
								 JackPortIsInput, 0);
	m_output = jack_port_register(m_client, "midi out", JACK_DEFAULT_MIDI_TYPE,
								  JackPortIsOutput, 0);

	jack_set_process_callback(m_client, process_wrapper, (void*)this);
}

void JackClient::start_processing()
{
	if (jack_activate(m_client))
		throw("Can't activeate jack client.\n");
}

int JackClient::process(jack_nframes_t frames)
{
    void* in_buffer = jack_port_get_buffer(m_input, frames);
    void* out_buffer = jack_port_get_buffer(m_output, frames);
    jack_midi_clear_buffer(out_buffer);

    jack_nframes_t event_count = jack_midi_get_event_count(in_buffer);
    jack_midi_event_t event;
	jack_midi_data_t velocity;
    for (jack_nframes_t i = 0; i < event_count; ++i) {
        jack_midi_event_get(&event, in_buffer, i);
        if ((event.buffer[0] & 0xf0) == 0x90) {
			// message is note on
			if ((event.buffer[0] & 0x0f) == m_midi_channel || m_midi_channel == -1)
			{
				// the message is on the midi channel we're editing or we're 
				// editing all midi channels
				jack_midi_data_t velocity = event.buffer[2];
				event.buffer[2] = m_velocity_curve->adjust_volume(velocity);
				if (m_logging)
				{
					logger.log(velocity, event.buffer[2]);
				}
			}
        }
        jack_midi_event_write(out_buffer, event.time, event.buffer, event.size);
    }
    return 0;
}

void JackClient::set_midi_channel(const int channel)
{
	m_midi_channel = channel;
}
