/* a file to compile Nuklear in so I don't have to keep recompiling it */
#define NK_IMPLEMENTATION
#define NK_GLFW_GL4_IMPLEMENTATION

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "nuklear.h"
#include "nuklear_glfw_gl4.h"
