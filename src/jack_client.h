#include <algorithm>
#include <jack/jack.h>
#include <jack/midiport.h>
#include <memory>
#include <string>
#include <utility>

#include "ring_buffer.h"
class MidiCurve;

const int LOGGER_BUFFER_SIZE = 256;

class RBLogger 
{
public:
	using midi_change_t = std::pair<jack_midi_data_t, jack_midi_data_t>;
	using log_buffer_t = RingBuffer<midi_change_t, LOGGER_BUFFER_SIZE>;

	RBLogger() : m_output{ nullptr } {}
	RBLogger(std::shared_ptr<log_buffer_t> &output) : m_output{ output } {}
	void log(jack_midi_data_t in, jack_midi_data_t out)
	{
		if (m_output) m_output->push(midi_change_t{ in, out }); 
	}
	void set_output(std::shared_ptr<log_buffer_t> &output) { m_output = output; }
protected:
	std::shared_ptr<log_buffer_t> m_output;
};

class JackClient
{
public:
	JackClient(std::shared_ptr<MidiCurve> &velocity_curve);
	~JackClient();
	void init();
	void start_processing();
	void set_midi_channel(const int channel);

	void set_logging(bool flag) { m_logging = flag; }
	RBLogger logger;
private:
	jack_port_t* m_input;
	jack_port_t* m_output;
	
	std::shared_ptr<MidiCurve> m_velocity_curve;
	jack_client_t* m_client;
	jack_status_t m_status;

	bool m_logging;

	int process(jack_nframes_t frames);

	static int process_wrapper(jack_nframes_t frames, void* arg)
	{
		auto client = (JackClient*)arg;
		return client->process(frames);
	}

	// midi channel can be 0-15 or -1 to send to all channels
	int m_midi_channel;
};

