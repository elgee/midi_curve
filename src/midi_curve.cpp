#include "midi_curve.h"
#include <cmath>
#include <functional>

/* TODO
 * - Need to clamp the parameters to sensible valaues that won't break the functions
 */

using midi_data_t = jack_midi_data_t;

MidiCurve::MidiCurve() : m_x0 { 0 },
                         m_y0 { 0 },
                         m_k { 0 },
                         m_has_changed { true },
                         m_buffer_ready { false }
{
    m_x1 = m_velocity_curve->size();
    m_y1 = m_x1;
    m_velocity_curve = &m_curve_buffer_0;
    m_buffer = &m_curve_buffer_1;
    calculate_curve();
    update_curve();
}

void MidiCurve::calculate_curve()
{ 
    using std::log2;
    using std::exp2;
	using std::fabs;

    if (!m_has_changed)
        return;
    // free parameters to fit curve to end points
    double a, b;
    std::function<midi_data_t(midi_data_t)> curve;
    // treat this as linear 
    if (m_k == 0) { 
        a = static_cast<double>(m_y1 - m_y0) / (m_x1 - m_x0);
        b = static_cast<double>(m_y0);
        curve = [&] (midi_data_t x) -> midi_data_t {
           return static_cast<midi_data_t>(a * x + b);
        };
    }
    else {
		double k{ fabs(m_k) };
        if (m_k > 0) {
            // exponential
			a = log2((m_y1 - m_y0) / (exp2(k * m_x1) - exp2(k * m_x0)));
			b = m_y0 - exp2(k * m_x0 + a);
            curve = [&] (midi_data_t in) { 
                auto x{ static_cast<double>(in) };
                return static_cast<midi_data_t>(exp2(m_k * x + a) + b);
            };
        }
        else {
            // logarithmic
			k = 1 / k;
			a = static_cast<double> (
					(m_x0 * exp2((m_y1 - m_y0) / k) - m_x1) / (1 - exp2((m_y1 - m_y0) / k)));
			b = static_cast<double> (m_y0 - k * log2(m_x0 + a));
            curve = [&] (midi_data_t in) {
				auto x{ static_cast<double>(in) };
				return static_cast<midi_data_t>(k * log2(x + a) + b);
			};
        }
    }

    for (midi_data_t in = 0; in < m_x1; ++in)
        (*m_buffer)[in] = curve(in);
    for (midi_data_t in = m_x1; in < m_buffer->size(); ++in)
        (*m_buffer)[in] = m_y1;

    m_has_changed = false;
    m_buffer_ready = true;
}

bool MidiCurve::update_curve()
{
    if (!m_buffer_ready)
        return false;

    midi_array_t* tmp{ m_velocity_curve };
    m_velocity_curve = m_buffer;
    m_buffer = tmp;

    m_buffer_ready = false;
	return true;
}
        
    
midi_data_t MidiCurve::adjust_volume(midi_data_t in)
{
    return (*m_velocity_curve)[in];
}
    
void MidiCurve::set_to_constant(midi_data_t velocity)
{
    m_k = 0;
    m_y1 = velocity;
    m_y0 = velocity;
    m_has_changed = true;
}
