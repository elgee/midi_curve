#include "nk_graph.h"
#include "nuklear.h"
#include <stdlib.h>
#include <cassert>

/*TODO 
 * make the line look better
 * can probably reduce the amount of commands necessary
 * make the history transparency reduce
 * make the history line the right way
 */

/* fill the graph struct g
   x_points:	the x coordinates
   y_points:	the y coordinates
   point_count: number of points
   x0, y0:		bottom left of graph
   x1, y1:		top right of graph
   returns 0 on success
*/
int make_graph(struct graph *g, const float *x_points, const float *y_points,
						int point_count, float x0, float y0, float x1, float y1)
{
	int i, ret = 1;
	
	/* reallocate memory for points if graph already has points and size is
	   diffrent. Otherwise we can just overwite the previous points.*/
	if (g->points && g->point_count != point_count) {
		free(g->points);
		g->points = (struct nk_vec2 *) malloc(point_count * sizeof(struct nk_vec2));
	}
	else if (!g->points) {
		g->points = (struct nk_vec2 *) malloc(point_count * sizeof(struct nk_vec2));
	}
    g->bounds = nk_rect(x0, y0, x1 - x0, y1 - y0);
    g->point_count = point_count;
	for (i = 0; i < point_count; ++i) {
		g->points[i] = nk_vec2(x_points[i], y_points[i]);
	}
	return 0;
}

void update_y_coordinates(struct graph *g, float *y_points)
{
	int i;
	for (i = 0; i < g->point_count; ++i)
		g->points[i].y = y_points[i];
}

/* transforms point which is positioned within bounds to point_ positioned in bounds_.
 * the y coordinate is also flipped for drawing.*/
int transform_point_to_bounds(const struct nk_vec2 *point, const struct nk_rect *bounds,
		struct nk_vec2 *point_, const struct nk_rect *bounds_)
{
	float scale_x = bounds_->w / bounds->w;
	float scale_y = bounds_->h / bounds->h;

	assert(point && bounds && point_ && bounds_);

	point_->x = point->x;
	point_->x -= bounds->x;
	point_->x *= scale_x;
	point_->x += bounds_->x;
	point_->y = point->y;
	point_->y -= bounds->y;
	point_->y *= scale_y;
	point_->y = bounds_->h - point_->y;
	point_->y += bounds_->y;

	return 0;
}
						  
int transform_graph_to_bounds(const struct graph *g, const struct nk_rect *new_bounds,
                               struct graph *g_)
{
    float scale_w = new_bounds->w / g->bounds.w;
    float scale_h = new_bounds->h / g->bounds.h;
    int i;

	assert(g);
	assert(new_bounds);
	assert(g_);

    g_->bounds = *new_bounds;
    g_->point_count = g->point_count;

    g_->points = (struct nk_vec2 *) malloc(g->point_count * sizeof(struct nk_vec2));
    for (i = 0; i < g->point_count; ++i) 
		transform_point_to_bounds(&g->points[i], &g->bounds, &g_->points[i], new_bounds);

    return 0;
}

int draw_graph(struct nk_command_buffer *out, const struct graph *g,
               const struct nk_rect *bounds, const struct nk_style *style)
{
    struct nk_vec2 scale;
    struct graph g_transformed;
	const struct nk_style_chart *style_chart;
	const float line_thickness = 4;
    int i;

	assert(style);
	style_chart = &(style->chart);
    scale.x = bounds->w / g->bounds.w;
    scale.y = bounds->h / g->bounds.h; 
    nk_fill_rect(out, *bounds, style_chart->rounding, 
			style_chart->background.data.color);
    nk_stroke_rect(out, *bounds, style_chart->rounding, style_chart->border,
			style_chart->border_color);
    transform_graph_to_bounds(g, bounds, &g_transformed);
    for (i = 0; i < g->point_count - 1; ++i)
        nk_stroke_line(out, g_transformed.points[i].x, g_transformed.points[i].y,
                       g_transformed.points[i+1].x, g_transformed.points[i+1].y,
                       line_thickness, style_chart->color);

    free(g_transformed.points);
    return 0;
}

/* gets the y coordinate of the point which has an x coordinate closest to the input
 * on the graph g if the graph g represents a monotonic function */
float graph_get_y(const struct graph *g, float x)
{
	int i = g->point_count / 2;
	int points_left = g->point_count;

	while(points_left > 1) {
		if (g->points[i-1].x > x) {
			points_left = i - 1;
			i /= 2;
		}
		else if (g->points[i-1].x < x) {
			points_left = g->point_count - i;
			i += points_left / 2;
		}
		else 
			return g->points[i].y;
	}
	return g->points[i].y;
}

/* highlights on the graph when a velocity is processed to indicate the change.*/
void draw_input(struct nk_command_buffer *out, const struct graph *g,
			   const float* inputs, const int count,
			   const struct nk_rect* bounds, const struct nk_style *style)
{
	int i;
	int j;
	struct nk_vec2 point;
	struct nk_vec2 point_;

	assert(out && g && inputs && bounds && style);
	assert(count > 0);
	
	for (i = 0; i < count; ++i) {
		point.x = inputs[i];
		if (point.x < g->bounds.x)
			point.x = g->bounds.x;
		if (point.x > g->bounds.x + g->bounds.w)
			point.x = g->bounds.x + g->bounds.w;
		point.y = graph_get_y(g, point.x);
		transform_point_to_bounds(&point, &g->bounds, &point_, bounds);
		nk_stroke_line(out, point_.x, bounds->y + bounds->h, point_.x, point_.y,
				2, style->chart.selected_color);
		nk_stroke_line(out, point_.x, point_.y, bounds->x, point_.y,
				2, style->chart.selected_color);
	}
}

void draw_processed(struct nk_command_buffer *out, const struct graph *g, 
		const struct nk_vec2* midi_changes, const int count, const int last_index,
		const struct nk_rect* bounds, const struct nk_style *style)
{
	int i;
	int j;
	struct nk_vec2 point_;
	struct nk_color color = style->chart.selected_color;
	nk_byte initial_alpha = color.a;

	assert(out && g && midi_changes && bounds && style);
	assert(count > 0);
	
	for (i = last_index; i > 0; --i) {
		transform_point_to_bounds(&midi_changes[i], &g->bounds, &point_, bounds);
		nk_stroke_line(out, point_.x, bounds->y + bounds->h, point_.x, point_.y,
				2, color);
		nk_stroke_line(out, point_.x, point_.y, bounds->x, point_.y,
				2, color);
		color.a /= 2;
	}
	for (i = count - 1; i > last_index; --i) {
		transform_point_to_bounds(&midi_changes[i], &g->bounds, &point_, bounds);
		nk_stroke_line(out, point_.x, bounds->y + bounds->h, point_.x, point_.y,
				2, color);
		nk_stroke_line(out, point_.x, point_.y, bounds->x, point_.y,
				2, color);
		color.a /= 2;
	}
}

int nk_do_graph(struct nk_context *ctx, const struct graph *g, bool do_draw_processed,
		const struct nk_vec2* midi_changes, const int count, const int last_index)
{
    struct nk_window *win;
    struct nk_panel *layout;
    struct nk_input *in;
    const struct nk_style *style;

    int ret = 0;
    float old_value;
    struct nk_rect bounds;
    enum nk_widget_layout_states state;

    assert(ctx);
    assert(ctx->current);
    assert(ctx->current->layout);
    if (!ctx || !ctx->current || !ctx->current->layout || !g)
        return ret;

    win = ctx->current;
    style = &ctx->style;
    layout = win->layout;

    /* maybe I'll do something with the state later */
    state = nk_widget(&bounds, ctx);
    if (!state) return ret;
    in = (/*state == NK_WIDGET_ROM || */ layout->flags & NK_WINDOW_ROM) ? 0 : &ctx->input;
    draw_graph(&win->buffer, g, &bounds, style);
	if (do_draw_processed)
		draw_processed(&win->buffer, g, midi_changes, count, last_index, &bounds, style);
	
	return 1;
}
