#pragma once

#include <jack/midiport.h>
#include <array>

using midi_data_t = jack_midi_data_t;
using midi_array_t = std::array<midi_data_t, 128>;

class MidiCurve 
{

public:
    MidiCurve();
    // calculate the new curve if any parameters haven't changed and store in the buffer
    void calculate_curve();
    // replace the curve with the curve calculated in the buffer
    bool update_curve();
    // return the volume as adjusted by the curve
    midi_data_t adjust_volume(midi_data_t in);

    void set_to_constant(midi_data_t velocity = 127);
    // setters
    void set_max_in(const midi_data_t max_in) { m_x1 = max_in; m_has_changed = true;}
    void set_min_out(const midi_data_t min_in) { m_y0 = min_in; m_has_changed = true;}
    void set_max_out(const midi_data_t max_out) { m_y1 = max_out; m_has_changed = true;}
    void set_adjustable_parameter(const double param) { m_k = param; m_has_changed = true;}
private:
    // a pointer to the curve array
    midi_array_t* m_velocity_curve;
    midi_array_t* m_buffer;
    // starting coordinates of curve x_0 will always be 0, y_0 is the minimum out
    midi_data_t m_x0, m_y0;
    // end coordinates of curve 
    midi_data_t m_x1, m_y1;
    // adjustable parameter
    double m_k;
    bool m_has_changed;
    
    midi_array_t m_curve_buffer_0;
    midi_array_t m_curve_buffer_1;
    bool m_buffer_ready;
};
