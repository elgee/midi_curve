#include "nuklear.h"
#include <cassert>

int limited_slider(struct nk_context *ctx, float min_value, float* value, float max_value, 
		float step,	float min_limit, float max_limit)
{
	int ret;
	float low_padding_ratio = (min_limit - min_value) / (max_value - min_value);
	float high_padding_ratio = (max_value - max_limit) / (max_value - min_value);

	assert(min_limit >= min_value && min_limit <= max_limit && max_limit <= max_value);

	nk_layout_row_begin(ctx, NK_DYNAMIC, 0, 3);
	{
		nk_layout_row_push(ctx, low_padding_ratio);
		nk_layout_row_push(ctx, 1.0f - low_padding_ratio - high_padding_ratio);
		ret = nk_slider_float(ctx, min_limit, value, max_limit, step);
		nk_layout_row_push(ctx, high_padding_ratio);
	}
	nk_layout_row_end(ctx);
	return ret;
}
