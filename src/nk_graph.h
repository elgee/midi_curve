#ifndef NK_GRAPH_H
#define NK_GRAPH_H

#include <stdlib.h>
#include "nuklear.h"

struct graph
{
    struct nk_rect bounds;
    struct nk_vec2 *points;
    int point_count;
};


int make_graph(struct graph *g, const float *x_points, const float *y_points,
		int point_count, float x0, float y0, float x1, float y1);

int nk_do_graph(struct nk_context *ctx, const struct graph *g, bool do_draw_processed,
		const struct nk_vec2* midi_changes, const int count, const int last_index);

void update_y_coordinates(struct graph *g, float *y_points);

//int transform_graph_to_bounds(const struct graph *g, const struct nk_rect *new_bounds,
//                               struct graph *g_);

//int draw_graph(struct nk_command_buffer *out, const struct graph *g,
//               const struct nk_rect *bounds, const struct nk_style *style=NULL);

#endif
