#include <cstddef>
#include <array>
#include <cassert>
#include <atomic>

template <class T, size_t N>
class RingBuffer 
{
public:
	RingBuffer() : m_data{}, m_head{0}, m_tail{0} {}
	
	T pop()
	{
		// don't allow popping of an empty buffer
		assert(m_head.load() != m_tail.load());
		size_t tail = m_tail.load();
		m_tail.store((++m_tail) % N);
		return m_data[tail];
	}

	void push(const T &item)
	{
		m_data[m_head.load()] = item;
		m_head.store((++m_head) % N);
		// for now hope the buffer is big enough
		assert(m_head.load() != m_tail.load());
	}

	void push(T &&item)
	{
		m_data[m_head.load()] = std::move(item);
		m_head.store((++m_head) % N);
		assert(m_head.load() != m_tail.load());
	}
	
	bool empty() { return m_head.load() == m_tail.load(); }

	size_t count()
	{
		size_t head = m_head.load();
		size_t tail = m_tail.load();
		if (head >= tail)
			return head - tail;
		else
			return N + head - tail;
	}
protected:
	std::array<T, N> m_data;
	std::atomic<size_t> m_head;
	std::atomic<size_t> m_tail;
};
